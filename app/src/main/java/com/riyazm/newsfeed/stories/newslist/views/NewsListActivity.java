package com.riyazm.newsfeed.stories.newslist.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.riyazm.newsfeed.APIService;
import com.riyazm.newsfeed.R;
import com.riyazm.newsfeed.stories.newsdetails.NewsDetailsActivity;
import com.riyazm.newsfeed.stories.newslist.helpers.MyAdapter;
import com.riyazm.newsfeed.stories.newslist.models.Article;
import com.riyazm.newsfeed.stories.newslist.models.LatestHeadLinesResponse;
import com.riyazm.newsfeed.utils.CustomItemClickListener;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsListActivity extends AppCompatActivity {

    public static final String TAG = NewsListActivity.class.getSimpleName();
    RecyclerView recyclerView;
    ArrayList<Article> listData = new ArrayList<>();
    MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        recyclerView = findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(listData, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(NewsListActivity.this, NewsDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("detail_url", listData.get(position).getUrl());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(mAdapter);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<LatestHeadLinesResponse> call = service.getMoviesList();
        call.enqueue(new Callback<LatestHeadLinesResponse>() {

            @Override
            public void onResponse(Call<LatestHeadLinesResponse> call, Response<LatestHeadLinesResponse> response) {
                // just to make sure http code is  in range 200..300
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "passed:" + response.body().getTotalResults() + " actual:" + response.body().getArticles().size());
                    listData.addAll(response.body().getArticles());
                    mAdapter.notifyDataSetChanged();

                } else {
                    Log.d(TAG, "bad response: http response code range not in 200..300: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<LatestHeadLinesResponse> call, Throwable t) {
                Log.d(TAG, "failed: " + t.getMessage());
            }
        });
    }
}
