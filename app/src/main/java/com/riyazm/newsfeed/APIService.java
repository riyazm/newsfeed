package com.riyazm.newsfeed;

import com.riyazm.newsfeed.stories.newslist.models.LatestHeadLinesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {
// https://newsapi.org/v2/top-headlines?country=us&apiKey=API_KEY
    @GET("v2/top-headlines?country=in&apiKey=c7c29066d8374c7b8525f67c529f7fb2")
    Call<LatestHeadLinesResponse> getMoviesList();
}
