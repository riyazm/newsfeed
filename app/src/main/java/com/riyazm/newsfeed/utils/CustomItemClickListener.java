package com.riyazm.newsfeed.utils;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
